import { IsArray, IsDataURI, IsDateString, IsDefined, IsEnum, IsNumber, IsString, ValidateNested } from "class-validator";
import { SensorType, SensorTypes } from "../helper/utils";

class DataDto {
    @IsDefined()
    @IsArray()
    @ValidateNested({each: true})
    @IsDateString()
    timeStamps!: Date[]

    @IsDefined()
    @IsArray()
    @ValidateNested({each: true})
    @IsNumber()
    values!: number[]
}

export class SensorDataDto {
    @IsDefined()
    @IsString()
    sensorName!: string;

    @IsDefined()
    @IsString()
    modulName!: string;

    @IsDefined()
    @IsEnum(SensorTypes)
    sensorType!: SensorType;

    @IsDefined()
    data!: DataDto;
}

