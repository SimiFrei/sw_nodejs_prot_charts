export declare type SensorType = 'temperature' | 'power'
export enum SensorTypes {
    TEMPERATURE = 'temperature',
    POWER = 'power'
}