import { Inject, Injectable, Logger, LoggerService } from '@nestjs/common';
import { ISensorData } from './_interface/sensorData.interface';

@Injectable()
export class SensorDataService {
    constructor(private data: ISensorData[],
        @Inject(Logger)
        private readonly logger: LoggerService){}

    public saveFakeData(data: ISensorData){
        this.logger.log(data);
       const sensorExists: boolean = this.data.some((d) => d.sensorName === data.sensorName && d.modulName === data.modulName);
       if(sensorExists){
            this.data = this.data.map((d) => {
                if(d.sensorName === data.sensorName && d.modulName === data.modulName){
                    const update: ISensorData = {
                        sensorName: d.sensorName,
                        modulName: d.modulName,
                        sensorType: d.sensorType,
                        data: {
                            timeStamps: [...d.data.timeStamps, ...data.data.timeStamps],
                            values: [...d.data.values, ...d.data.values]
                        }
                    }
                    return update;
                }
                    return d;
            })
       }else{
        this.data.push(data);
       }
    }

    public getFakeData(): ISensorData[] {
        this.logger.log(this.data);
        return this.data;
    }
}
