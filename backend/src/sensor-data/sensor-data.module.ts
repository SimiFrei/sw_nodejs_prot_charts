import { Logger, Module } from '@nestjs/common';
import { SensorDataController } from './sensor-data.controller';
import { SensorDataService } from './sensor-data.service';

@Module({
  controllers: [SensorDataController],
  providers: [SensorDataService, Array, Logger],
  exports: [SensorDataModule],
})
export class SensorDataModule {}
