import { Body, Controller, Get, Post, Query, ValidationPipe } from '@nestjs/common';
import { SensorDataService } from './sensor-data.service';
import { ISensorData } from './_interface/sensorData.interface';
import { ApiBody, ApiTags } from '@nestjs/swagger';
import { SensorDataDto } from './dto/sensorData.dto';

@ApiTags('Sensor-Data')
@Controller('sensor-data')
export class SensorDataController {
    constructor(private readonly sensorDataService: SensorDataService) {}

    @Get()
    getData(): ISensorData[] {
        return this.sensorDataService.getFakeData();
    }

    @Post()
    @ApiBody({ type: SensorDataDto})
    public setData(@Body(ValidationPipe) data: SensorDataDto){
        return this.sensorDataService.saveFakeData(data);
    }
}
