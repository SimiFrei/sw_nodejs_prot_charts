import { SensorType } from "../helper/utils";

export interface ISensorData {
    sensorName: string;
    modulName: string;
    sensorType: SensorType;
    data: {
        timeStamps: Date[],
        values: number[]
    }
}