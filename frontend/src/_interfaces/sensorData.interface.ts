import { SensorType } from "../helpers/utils";

export interface ISensorData {
    sensorName: string;
    modulName: string;
    sensorType: SensorType;
    data: {
        timeStamps: Date[],
        values: number[]
    }
}