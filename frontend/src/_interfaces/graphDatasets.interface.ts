export interface IDatasets {
    label: string;
    data: number[]
    fill: boolean;
    borderColor: string;
    tension: number;
}