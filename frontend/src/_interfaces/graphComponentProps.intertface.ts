import { ChartData, ChartOptions, CoreChartOptions, DatasetChartOptions, ElementChartOptions, LineControllerChartOptions, LinearScaleOptions, PluginChartOptions, Point, ScaleChartOptions, ScatterControllerChartOptions } from "chart.js";
import { IDatasets } from "./graphDatasets.interface";
import { _DeepPartialObject } from "chart.js/dist/types/utils";

export interface IGraphComponentProps {
    data: ChartData<"line">
    options?: ChartOptions
}

export interface IGraphComponentData {
    labels: string[],
    datasets: IDatasets[];
}
