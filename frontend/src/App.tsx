import React from 'react';
import './App.css';
import GraphComponent from './components/graph/graph.component';
import { ChartData, ChartOptions } from 'chart.js';
import axios from 'axios';
import { ISensorData } from './_interfaces/sensorData.interface';
import { ChartDataset } from 'chart.js';
import { SensorTypes } from './helpers/utils';
import { UseQueryResult, useQuery } from 'react-query';

const dataX: ChartData<"line"> = {
  labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
  datasets: [
    {
      label: 'Sample Dataset',
      data: [65, 59, 80, 81, 56, 55, 40],
      fill: false,
      borderColor: 'rgb(75, 192, 192)',
      tension: 0.1,
      yAxisID: 'y',
    },
    {
      label: 'Dataset 2',
      data: [165, 200, 180, 560, 156, 155, 140],
      fill: false,
      borderColor: 'rgb(255, 192, 192)',
      tension: 0.1,
      yAxisID: 'y1'
    },
  ],
};





function App() {
  return (
    <div className="App">
      <header className="App-header">
        <GraphComponent/>
        {/* <GraphComponent data={dataX} options={options} /> */}
      </header>
    </div>
  );
}

export default App;
