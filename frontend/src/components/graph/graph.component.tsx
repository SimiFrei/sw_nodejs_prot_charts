import { CategoryScale, ChartData, ChartDataset, ChartOptions, Interaction, Legend, LineElement, LinearScale, PointElement, Title } from "chart.js";
import React from "react";
import { Line } from "react-chartjs-2";
import { IGraphComponentProps } from "../../_interfaces/graphComponentProps.intertface";
import { Chart } from "chart.js";
import { Tooltip } from "chart.js";
import axios from "axios";
import { useQuery } from "react-query";
import { ISensorData } from "../../_interfaces/sensorData.interface";
import { SensorTypes } from "../../helpers/utils";
import { title } from "process";

Chart.register(LinearScale);
Chart.register(CategoryScale);
Chart.register(PointElement);
Chart.register(LineElement);
Chart.register(Title);
Chart.register(Legend);
Chart.register(Tooltip)
const GraphComponent: React.FC = () => {

    const dataX: ChartData<"line"> = {
        labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
        datasets: [
            {
                label: 'Sample Dataset',
                data: [65, 59, 80, 81, 56, 55, 40],
                fill: false,
                borderColor: 'rgb(75, 192, 192)',
                tension: 0.1,
                yAxisID: 'y',
            },
            {
                label: 'Dataset 2',
                data: [165, 200, 180, 560, 156, 155, 140],
                fill: false,
                borderColor: 'rgb(255, 192, 192)',
                tension: 0.1,
                yAxisID: 'y1'
            },
        ],
    };

    let options: ChartOptions<"line"> = {
        responsive: true,
        scales: {
            y: {
                type: 'linear',
                display: true,
                position: 'left',
            },
            y1: {
                type: 'linear',
                display: true,
                position: 'right',

                // grid line settings
                grid: {
                    drawOnChartArea: false, // only want the grid lines for one axis to show up
                },
            },
        },
        plugins: {
            title: {
                display: true,
                text: 'Custom Chart Title'
            },
            tooltip: {
                mode: 'nearest',
                axis: 'xy',
                intersect: false, // false --> tooltip appears everytime clicked on the graph on the nearest point
            }
        },
    };

    let colors: { sensor: string, color: string }[] = []
    const axiosInstance = axios.create({
        baseURL: 'http://192.168.145.134:3001/api',
        headers: {
            'Content-Type': 'application/json', // Set the default content type
            // Add any other default headers you require
        },
    })
    const fetchSensorData = async (): Promise<ChartData<"line">> => {
        const response = await axiosInstance.get('/sensor-data');
        const data: ISensorData[] = response.data;
        console.log(response.data);
        const dataSets: ChartDataset<"line">[] = []
        data.filter((s) => s.modulName === '1').forEach((s: ISensorData) => {
            let color: string = ''
            if (colors.some((c) => c.sensor === s.sensorName)) {
                color = colors.filter((c) => c.sensor === s.sensorName)[0].color;
            } else {
                color = `rgb(${Math.floor(Math.random() * 256)}, ${Math.floor(Math.random() * 256)}, ${Math.floor(Math.random() * 256)})`
                colors.push({ sensor: s.sensorName, color })
            }
            console.log(colors);
            dataSets.push({
                label: `${s.sensorName} [${s.sensorType}]`,
                data: s.data.values,
                fill: false,
                borderColor: colors.filter((c) => c.sensor === s.sensorName)[0].color,
                tension: 0.1,
                yAxisID: s.sensorType === SensorTypes.TEMPERATURE ? 'y' : 'y1',
            })
        })
        return {
            labels: data.length > 0 ? data[0].data.timeStamps : [],
            datasets: dataSets
        }
    }

    const { data } = useQuery<ChartData<"line">>({ queryKey: 'sensor-data', queryFn: fetchSensorData })
    console.log(data);
    return <Line data={data ? data : dataX} options={{...options, plugins: {title: {text: 'Böxli 1', display: true,}}}} />;
}

export default GraphComponent;